function checkInputHasValue (event) {
    let parentEle = event.target.parentElement;
    if (event.target.value.length > 0) {
      addClass(parentEle, "has-value");
    }
    else{
      removeClass(parentEle, "has-value");
    }
  };

  function addClass(element, eleClass) {
    let getElement = element, getClass = eleClass, arr;
    arr = getElement.className.split(" ");
    if (arr.indexOf(getClass) == -1) {
      getElement.className += " " + getClass;
    }
  }

  function removeClass(element,eleClass) {
    element.classList.remove(eleClass);
  }

  function eyeToggle(event) {
    toggleCssClass(event.target, "eyeToggle");
  }
  function toggleCssClass(element, eleClass) {
    element.classList.toggle(eleClass);
  }
  export {checkInputHasValue , addClass, removeClass, toggleCssClass, eyeToggle};